import {
  CREATE_USER,
  LOGIN_USER,
  CHANGE_USER,
  DELETE_USER,
} from './constants';

export function createUser(email, password) {
  return {
    type: CREATE_USER,
    email,
    password,
  };
}

export function loginUser(email, password) {
  return {
    type: LOGIN_USER,
    email,
    password,
  };
}

export function deleteUser(id) {
  return {
    type: DELETE_USER,
    id,
  };
}

export function changeUser(obj) {
  return {
    type: CHANGE_USER,
    obj,
  };
}
