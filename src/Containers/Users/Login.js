import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import Snackbar from 'material-ui/Snackbar';
import LoginForm from './../../Components/User/LoginForm';
import { loginUser } from './actions';
import { withCurrentUser } from './gql';

class Login extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    user: PropTypes.object,
    onLogin: PropTypes.func.isRequired,
    usersStore: PropTypes.object.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    user: { id: undefined, email: undefined, isManager: undefined },
  }

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      form: {
        email: '',
        password: '',
      },
    };
  }
  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      form: { ...this.state.form, [name]: value },
    });
  }
  onLogin = () => {
    const { email, password } = this.state.form;
    this.props.onLogin(email, password);
    this.setState({ redirect: true });
  }
  render() {
    const { email, password } = this.state.form;
    const { user, usersStore } = this.props;
    const isLogged = !!(user && user.id);
    if (usersStore.loginOk) {
      this.props.history.push('/');
    }
    return (
      <div>
        { !isLogged &&
          <LoginForm
            email={email}
            password={password}
            onLogin={this.onLogin}
            onChange={this.onChange}
          />
        }
        <Snackbar
          open={usersStore.loginError}
          message="Could not log in... check if user and password are correct!"
          autoHideDuration={5000}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    usersStore: state.usersStore,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onLogin: (email, password) => {
      dispatch(loginUser(email, password));
    },
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  withCurrentUser,
)(Login);

