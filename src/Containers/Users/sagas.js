import { takeEvery, call, put } from 'redux-saga/effects';

import {
  CREATE_USER,
  LOGIN_USER,
  DELETE_USER,
  CHANGE_USER,
  TRY_LOGIN,
  LOGIN_OK,
  LOGIN_ERROR,
} from './constants';

import {
  createUserMutation,
  loginUserMutation,
  deleteUserMutation,
  changeUserMutation,
} from './gql';

function* createUser(action) {
  try {
    yield put({ type: TRY_LOGIN });
    yield call(createUserMutation, action.email, action.password);
    yield put({ type: LOGIN_OK });
    yield call(loginUser, action);
  } catch (e) {
    console.log(e);
    yield put({ type: LOGIN_ERROR });
  }
}

function* loginUser(action) {
  try {
    yield put({ type: TRY_LOGIN });
    const { data } = yield call(loginUserMutation, action.email, action.password);
    yield localStorage.setItem('authIdToken', data.signinUser.token);
    yield call(() => window.location.reload());
    yield put({ type: LOGIN_OK });
  } catch (e) {
    console.log(e);
    yield put({ type: LOGIN_ERROR });
  }
}

function* deleteUser(action) {
  yield call(deleteUserMutation, action.id);
}

function* changeUser(action) {
  yield call(changeUserMutation, action.obj);
}

export function* createUserSaga() {
  yield takeEvery(CREATE_USER, createUser);
}

export function* loginUserSaga() {
  yield takeEvery(LOGIN_USER, loginUser);
}

export function* deleteUserSaga() {
  yield takeEvery(DELETE_USER, deleteUser);
}

export function* changeUserSaga() {
  yield takeEvery(CHANGE_USER, changeUser);
}

