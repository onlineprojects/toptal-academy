export const CREATE_USER = 'users/CREATE_USER';
export const LOGIN_USER = 'users/LOGIN_USER';
export const DELETE_USER = 'users/DELETE_USER';
export const CHANGE_USER = 'users/CHANGE_USER';
export const TRY_LOGIN = 'users/TRY_LOGIN';
export const LOGIN_OK = 'users/LOGIN_OK';
export const LOGIN_ERROR = 'users/LOGIN_ERROR';

