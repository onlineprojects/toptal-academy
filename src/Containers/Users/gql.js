import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import client from './../../apollo';

export const CREATE_USER_GQL = gql`
  mutation ($email: String!, $password: String!) {
    createUser(
      authProvider: {
        email: {
          email: $email,
          password: $password
        }
      }
    ){
    id
    }
  }
`;

export const LOGIN_USER_GQL = gql`
  mutation ($email: String!, $password: String!) {
    signinUser(email: {email: $email, password: $password}) {
      token
    }
  }
`;

export const DELETE_USER_GQL = gql`
  mutation ($id: ID!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export const CHANGE_USER_GQL = gql`
  mutation ($id: ID!, $isManager: Boolean!) {
    updateUser(id: $id, isManager: $isManager) {
      id
    }
  }
`;

export const QUERY_CURRENT_USER_GQL = gql`
  query currentUser {
    user {
      id
      email
      isManager
    }
  }
`;

export const QUERY_ALL_USERS_GQL = gql`
  query getUsers {
    allUsers {
      id
      email
      isManager
    }
  }
`;

export function createUserMutation(email, password) {
  return client.mutate({
    mutation: CREATE_USER_GQL,
    variables: { email, password },
  });
}

export function loginUserMutation(email, password) {
  return client.mutate({
    mutation: LOGIN_USER_GQL,
    variables: { email, password },
    refetchQueries: ['currentUser'],
  });
}

export function deleteUserMutation(id) {
  return client.mutate({
    mutation: DELETE_USER_GQL,
    variables: { id },
    refetchQueries: ['getUsers'],
  });
}

export function changeUserMutation({ id, isManager }) {
  return client.mutate({
    mutation: CHANGE_USER_GQL,
    variables: { id, isManager },
    refetchQueries: ['getUsers'],
  });
}

export const withUsers = graphql(QUERY_ALL_USERS_GQL, {
  props: ({ data: { allUsers } }) => ({
    users: allUsers,
  }),
});

export const withCurrentUser = graphql(QUERY_CURRENT_USER_GQL, {
  options: { fetchPolicy: 'network-only' },
  props: ({ data: { user, loading } }) => ({
    user,
    listLoading: loading,
  }),
});
