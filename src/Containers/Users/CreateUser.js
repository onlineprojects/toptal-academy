import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import Snackbar from 'material-ui/Snackbar';
import CreateUserForm from './../../Components/User/CreateUserForm';
import { createUser, loginUser } from './actions';
import { withCurrentUser } from './gql';

class CreateUser extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    user: PropTypes.object,
    onSave: PropTypes.func.isRequired,
    usersStore: PropTypes.object.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    user: { id: undefined, email: undefined, isManager: undefined },
  }

  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: '',
        password: '',
      },
    };
  }
  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      form: { ...this.state.form, [name]: value },
    });
  }
  onSave = () => {
    const { email, password } = this.state.form;
    const { user } = this.props;
    this.props.onSave(email, password, (user && !user.isManager));
  }
  render() {
    const { email, password } = this.state.form;
    const { usersStore } = this.props;
    if (usersStore.loginOk) {
      this.props.history.push('/');
    }
    return (
      <div>
        <CreateUserForm
          email={email}
          password={password}
          onSave={this.onSave}
          onChange={this.onChange}
        />
        <Snackbar
          open={usersStore.loginError}
          message="Could not create user... try a different email!"
          autoHideDuration={5000}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    usersStore: state.usersStore,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSave: (email, password, login) => {
      dispatch(createUser(email, password, login));
    },
    onLogin: (email, password) => {
      dispatch(loginUser(email, password));
    },
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  withCurrentUser,
)(CreateUser);

