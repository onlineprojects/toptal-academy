import {
  TRY_LOGIN,
  LOGIN_OK,
  LOGIN_ERROR,
} from './constants';

const initialState = {
  loginError: false,
  loginOk: false,
};

function usersReducer(state = initialState, action) {
  switch (action.type) {
    case TRY_LOGIN:
      return { loginError: false, loginOk: false };
    case LOGIN_OK:
      return { loginError: false, loginOk: true };
    case LOGIN_ERROR:
      return { loginError: true, loginOk: false };
    default:
      return state;
  }
}

export default usersReducer;
