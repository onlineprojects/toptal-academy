import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import ListUsers from './../../Components/User/ListUsers';
import { deleteUser, changeUser } from './actions';
import { withCurrentUser, withUsers } from './gql';

class Users extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    users: PropTypes.array,
    user: PropTypes.object,
    onDelete: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    users: [],
    user: { id: undefined, email: undefined, isManager: undefined },
  }

  constructor(props) {
    super(props);
    this.onToggle = this.onToggle.bind(this);
  }

  onToggle(id, isManager) {
    this.props.onChange({ id, isManager: !(isManager) });
  }

  onCreate = () => {
    this.props.history.push('/users/new');
  }

  render() {
    const { users, user } = this.props;
    return (
      <div>
        { user && user.isManager &&
          <ListUsers
            users={users}
            onDelete={this.props.onDelete}
            onToggle={this.onToggle}
            onCreate={this.onCreate}
          />
        }
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onDelete: (id) => {
      dispatch(deleteUser(id));
    },
    onChange: (obj) => {
      dispatch(changeUser(obj));
    },
  };
}

export default compose(
  connect(undefined, mapDispatchToProps),
  withRouter,
  withCurrentUser,
  withUsers
)(Users);

