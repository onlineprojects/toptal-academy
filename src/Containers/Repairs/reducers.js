import {
  GET_REPAIRS,
} from './constants';

const initialState = {
  state: 'todo',
};

function repairsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_REPAIRS:
      return state;
    default:
      return state;
  }
}

export default repairsReducer;
