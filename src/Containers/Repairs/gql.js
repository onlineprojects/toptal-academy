import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import client from './../../apollo';

export const CREATE_REPAIR_GQL = gql`
  mutation (
    $userId: ID,
    $scheduleDate: DateTime!,
    $scheduleTime: Int!
    $isComplete: Boolean,
    $isApproved: Boolean
  ){
    createRepair(
      userId: $userId,
      scheduleDate: $scheduleDate,
      scheduleTime: $scheduleTime,
      isComplete: $isComplete,
      isApproved: $isApproved
    ){
      id
    }
  }
`;

export const UPDATE_REPAIR_GQL = gql`
  mutation (
    $id: ID!,
    $userId: ID,
    $scheduleDate: DateTime!,
    $scheduleTime: Int!
    $isComplete: Boolean,
    $isApproved: Boolean
  ){
    updateRepair(
      id: $id,
      userId: $userId,
      scheduleDate: $scheduleDate,
      scheduleTime: $scheduleTime,
      isComplete: $isComplete,
      isApproved: $isApproved
    ){
      id
    }
  }
`;

export const DELETE_REPAIR_GQL = gql`
  mutation ($id: ID!) {
    deleteRepair(id: $id) {
      id
    }
  }
`;

export const QUERY_ALL_REPAIRS_GQL = gql`
  query getRepairs (
    $dateEnd: DateTime,
    $timeStart: Int,
    $timeEnd: Int,
    $userId: ID,
    $isComplete: Boolean
  ) {
    allRepairs (
      filter: {
        scheduleDate_lte: $dateEnd,
        AND: {
          scheduleTime_gte: $timeStart,
          scheduleTime_lte: $timeEnd
        },
        user: { id: $userId },
        isComplete: $isComplete
      }
    ){
      id
      scheduleDate
      scheduleTime
      isComplete
      isApproved
      user {
        id
        email
      }
    }
  }
`;

export const QUERY_REPAIR_GQL = gql`
  query getRepair($id: ID!) {
    Repair (id: $id) {
      id
      scheduleDate
      scheduleTime
      isComplete
      isApproved
      user {
        id
        email
      }
      comments {
        id
        comment
        user {
          id
          email
        }
      }
    }
  }
`;

export const CREATE_COMMENT_GQL = gql`
  mutation createComment (
    $repairId: ID,
    $userId:ID,
    $comment: String) {
    createComment(
      repairId: $repairId,
      userId: $userId,
      comment: $comment) {
      id
    }
  }
`;

export const DELETE_COMMENT_GQL = gql`
  mutation deleteComment ($id: ID) {
    deleteComment(id: $id) {
      id
    }
  }
`;

export const CHECK_SLOT_GQL = gql`
  query checkSlot (
    $dateStart: DateTime,
    $dateEnd: DateTime,
    $time: Int,
    $repairId: ID
    ) {
      allRepairs(
        filter: {
          scheduleTime: $time,
          scheduleDate_gte: $dateStart,
          scheduleDate_lte: $dateEnd,
          id_not: $repairId
    }) {
      id
      scheduleDate
      scheduleTime
    }
  }
`;

// ADHOC
export function createRepairMutation({
  userId,
  scheduleDate,
  scheduleTime,
  isComplete,
  isApproved }) {
  return client.mutate({
    mutation: CREATE_REPAIR_GQL,
    variables: {
      userId,
      scheduleDate,
      scheduleTime,
      isComplete,
      isApproved,
    },
    refetchQueries: ['getRepairs'],
  });
}

export function updateRepairMutation({
  id,
  userId,
  scheduleDate,
  scheduleTime,
  isComplete,
  isApproved }) {
  return client.mutate({
    mutation: UPDATE_REPAIR_GQL,
    variables: {
      id,
      userId,
      scheduleDate,
      scheduleTime,
      isComplete,
      isApproved,
    },
    refetchQueries: ['getRepairs'],
  });
}

export function deleteRepairMutation(id) {
  return client.mutate({
    mutation: DELETE_REPAIR_GQL,
    variables: { id },
  });
}

export function createCommentMutation(
  {
    repairId,
    userId,
    comment,
  }) {
  return client.mutate({
    mutation: CREATE_COMMENT_GQL,
    variables: {
      repairId,
      userId,
      comment,
    },
    refetchQueries: ['getRepair'],
  });
}

export function deleteCommentMutation(id) {
  return client.mutate({
    mutation: DELETE_COMMENT_GQL,
    variables: { id },
    refetchQueries: ['getRepair'],
  });
}

export function checkSlotQuery(dateStart, dateEnd, time, repairId) {
  return client.query({
    query: CHECK_SLOT_GQL,
    variables: { dateStart, dateEnd, time, repairId },
    fetchPolicy: 'network-only',
  });
}


// CONNECTs
export const withRepairs = graphql(QUERY_ALL_REPAIRS_GQL, {
  options: (ownProps) => ({
    variables: {
      dateEnd: undefined,
      timeStart: undefined,
      timeEnd: undefined,
      userId: (ownProps.user && ownProps.user.isManager)
        ? undefined
        : ((ownProps.user) ? ownProps.user.id : 'dontFetch'),
      isComplete: undefined,
    },
    fetchPolicy: 'network-only',
  }),
  props: ({ data }) => ({
    repairs: data.allRepairs,
    data,
  }),
});

export const withActiveRepair = graphql(QUERY_REPAIR_GQL, {
  options: (ownProps) => ({
    variables: {
      id: ownProps.match.params.id,
    },
    fetchPolicy: 'network-only',
  }),
  skip: (ownProps) => !ownProps.match.params.id,
  props: ({ data: { Repair } }) => ({
    repair: Repair,
  }),
});
