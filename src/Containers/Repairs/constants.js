export const GET_REPAIRS = 'repairs/GET_REPAIRS';
export const GET_REPAIR = 'repairs/GET_REPAIR';
export const CREATE_REPAIR = 'repairs/CREATE_REPAIR';
export const UPDATE_REPAIR = 'repairs/UPDATE_REPAIR';
export const DELETE_REPAIR = 'repairs/DELETE_REPAIR';
export const CREATE_COMMENT = 'repairs/CREATE_COMMENT';
export const DELETE_COMMENT = 'repairs/DELETE_COMMENT';
