import { takeEvery, call } from 'redux-saga/effects';

import {
  CREATE_REPAIR,
  DELETE_REPAIR,
  UPDATE_REPAIR,
  CREATE_COMMENT,
  DELETE_COMMENT,
} from './constants';

import {
  createRepairMutation,
  deleteRepairMutation,
  updateRepairMutation,
  createCommentMutation,
  deleteCommentMutation,
} from './gql';

function* createRepair(action) {
  const { data } = yield call(createRepairMutation, action.repair);
  console.log(data);
}

function* updateRepair(action) {
  const { data } = yield call(updateRepairMutation, action.repair);
  console.log(data);
}

function* deleteRepair(action) {
  const { data } = yield call(deleteRepairMutation, action.id);
  console.log(data);
}

function* createComment(action) {
  const { data } = yield call(createCommentMutation, action.comment);
  console.log(data);
}

function* deleteComment(action) {
  const { data } = yield call(deleteCommentMutation, action.id);
  console.log(data);
}


export function* createRepairSaga() {
  yield takeEvery(CREATE_REPAIR, createRepair);
}

export function* updateRepairSaga() {
  yield takeEvery(UPDATE_REPAIR, updateRepair);
}

export function* deleteRepairSaga() {
  yield takeEvery(DELETE_REPAIR, deleteRepair);
}

export function* createCommentSaga() {
  yield takeEvery(CREATE_COMMENT, createComment);
}

export function* deleteCommentSaga() {
  yield takeEvery(DELETE_COMMENT, deleteComment);
}
