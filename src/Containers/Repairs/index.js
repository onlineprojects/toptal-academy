import React, { PropTypes } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import Divider from 'material-ui/Divider';
import Filters from './../../Components/Repairs/Filters';
import ListRepairs from './../../Components/Repairs/ListRepairs';
import { withRepairs } from './gql';
import { withUsers, withCurrentUser } from './../Users/gql';

class Repairs extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    repairs: PropTypes.array,
    data: PropTypes.object,
    users: PropTypes.array,
    user: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    repairs: [],
    test: 123,
    data: {},
    users: [],
    user: { id: undefined, email: undefined, isManager: undefined },
  }

  constructor(props) {
    super(props);
    this.state = {
      filters: {
        dateEnd: undefined,
        timeStart: 0,
        timeEnd: 23,
        isComplete: undefined,
      },
    };
  }

  onEdit = (id) => {
    this.props.history.push(`/repairs/${id}`);
  }

  onChange = (e, value) => {
    const name = (e.target) ? e.target.name : e;
    this.setState({
      filters: {
        ...this.state.filters,
        [name]: value,
      },
    }, () => this.refetch());
  }

  onSelectUser = (chosenUser) => {
    if (chosenUser.id) {
      this.setState({
        filters: { ...this.state.filters,
          userId: chosenUser.id,
        },
      }, () => this.refetch());
    }
  }

  refetch = () => {
    this.props.data.refetch({ ...this.state.filters });
  }

  render() {
    const { repairs, user } = this.props;
    return (
      <div>
        { user && user.id &&
          <div>
            <h2>Repairs</h2>
            <Filters
              filters={this.state.filters}
              users={this.props.users}
              onSelectUser={this.onSelectUser}
              onChange={this.onChange}
              isManager={this.props.user.isManager}
            />
            <br />
            <Divider />
            <br />
            <ListRepairs
              repairs={repairs}
              onEdit={this.onEdit}
              isManager={this.props.user.isManager}
            />
          </div>
        }
      </div>
    );
  }
}

export default compose(
  withRouter,
  withUsers,
  withCurrentUser,
  withRepairs
)(Repairs);

