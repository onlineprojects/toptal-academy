import {
  CREATE_REPAIR,
  UPDATE_REPAIR,
  DELETE_REPAIR,
  CREATE_COMMENT,
  DELETE_COMMENT,
} from './constants';

export function createRepair(repair) {
  return {
    type: CREATE_REPAIR,
    repair,
  };
}

export function updateRepair(repair) {
  return {
    type: UPDATE_REPAIR,
    repair,
  };
}

export function deleteRepair(id) {
  return {
    type: DELETE_REPAIR,
    id,
  };
}

export function createComment(comment) {
  return {
    type: CREATE_COMMENT,
    comment,
  };
}

export function deleteComment(id) {
  return {
    type: DELETE_COMMENT,
    id,
  };
}

