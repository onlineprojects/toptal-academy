import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import Snackbar from 'material-ui/Snackbar';
import RepairForm from './../../Components/Repairs/RepairForm';
import Comments from './../../Components/Comments';
import { createRepair, updateRepair, createComment, deleteComment } from './actions';
import { withActiveRepair, checkSlotQuery } from './gql';
import { withUsers, withCurrentUser } from './../Users/gql';

class RepairPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    onSave: PropTypes.func.isRequired,
    onCreateComment: PropTypes.func.isRequired,
    repair: PropTypes.object,
    user: PropTypes.object,
    users: PropTypes.array,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    match: PropTypes.object,
  };

  static defaultProps = {
    repair: {},
    users: [],
    user: { id: undefined, email: undefined, isManager: undefined },
    match: {},
  }

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      form: {
        id: '',
        scheduleDate: '',
        scheduleTime: 10,
        isComplete: false,
        dtScheduleDate: new Date(),
        user: {
          id: '',
          email: '',
        },
      },
      newComment: '',
      errorMsg: {
        enabled: false,
        text: '',
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.repair && nextProps.repair.scheduleDate) { // it may be setting state twice
      this.setState({ form: {
        ...nextProps.repair,
        dtScheduleDate: new Date(nextProps.repair.scheduleDate),
        userId: (nextProps.repair && nextProps.repair.user && nextProps.repair.user.id)
          ? nextProps.repair.user.id : '',
        alreadyComplete: (nextProps.repair.isComplete),
      } });
    }
  }

  onChange = (e, value) => {
    const name = (e) ? e.target.name : 'dtScheduleDate';
    this.setState({
      form: {
        ...this.state.form,
        [name]: value,
      },
    });
  }

  onSelectUser = (chosenUser) => {
    if (chosenUser.id) {
      this.setState({
        form: {
          ...this.state.form,
          user: chosenUser },
      });
    }
  }

  onSave = async () => {
    const { form } = this.state;
    const userId = (this.props.match.params.id === 'new')
      ? this.props.user.id
      : form.user.id;
    if (!userId) {
      this.setState({ errorMsg: {
        enabled: true,
        text: 'User must be informed' } });
      return;
    }
    const dateStart = new Date(form.dtScheduleDate);
    dateStart.setUTCHours(0, 0, 0, 0);
    const dateEnd = new Date(form.dtScheduleDate);
    dateEnd.setUTCHours(23, 59, 59, 999);

    // validate slot duplication
    const { data } = await checkSlotQuery(
      dateStart,
      dateEnd,
      Number(form.scheduleTime),
      form.id
    );
    if (data.allRepairs[0]) {
      this.setState({ errorMsg: {
        enabled: true,
        text: 'We have a repair schedulted for this time, please select another time!' } });
      return;
    }
    const saveRepair = {
      id: form.id || '',
      userId,
      scheduleDate: dateStart,
      scheduleTime: Number(form.scheduleTime),
      isComplete: form.isComplete,
      isApproved: form.isApproved || false,
    };
    this.props.onSave(saveRepair);
    this.setState({ redirect: true });
  }

  onEditComment = (e, value) => {
    this.setState({ newComment: value });
  }

  onCreateComment = () => {
    const { form, newComment } = this.state;
    const newCommentObj = {
      userId: this.props.user.id,
      repairId: form.id,
      comment: newComment,
    };
    this.props.onCreateComment(newCommentObj);
  }

  handleRequestClose = () => {
    this.setState({ errorMsg: { enabled: false, text: '' } });
  };

  render() {
    const { redirect } = this.state;
    if (redirect) {
      this.props.history.push('/repairs');
    }
    const { repair, users, user } = this.props;
    return (
      <div>
        <RepairForm
          repair={this.state.form}
          users={users}
          user={user}
          onSave={this.onSave}
          onChange={this.onChange}
          onSelectUser={this.onSelectUser}
        />
        { repair && repair.comments &&
          <Comments
            comments={repair.comments}
            onChange={this.onEditComment}
            onCreate={this.onCreateComment}
          />
        }
        <Snackbar
          open={this.state.errorMsg.enabled}
          message={this.state.errorMsg.text}
          autoHideDuration={5000}
          onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSave: (repair) => {
      if (repair.id && (repair.id !== '')) {
        dispatch(updateRepair(repair));
      } else {
        dispatch(createRepair(repair));
      }
    },
    onCreateComment: (comment) => {
      dispatch(createComment(comment));
    },
    onDeleteComment: (id) => {
      dispatch(deleteComment(id));
    },
  };
}

export default compose(
  connect(undefined, mapDispatchToProps),
  withRouter,
  withActiveRepair,
  withUsers,
  withCurrentUser,
)(RepairPage);
