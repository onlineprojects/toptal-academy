import React, { PropTypes } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { withCurrentUser } from './../Users/gql';
import MainHeader from './../../Components/HomePage/MainHeader';

class Header extends React.PureComponent {
  static propTypes = {
    user: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };
  static defaultProps = {
    user: {},
  };

  logout = () => {
    window.localStorage.removeItem('authIdToken');
    window.location.reload();
    this.props.history.push('/');
  }
  render() {
    const { user } = this.props;
    const authRole = (user) ? user.isManager : undefined;
    return (
      <MainHeader authRole={authRole} logout={this.logout} user={user} />
    );
  }
}

export default compose(
  withRouter,
  withCurrentUser,
)(Header);
