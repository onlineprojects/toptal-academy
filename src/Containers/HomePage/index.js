import React from 'react';

class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <br />
        <p>
          Toptal Academy Project - React
        </p>
        <p>
          Author: <strong>Anderson Luiz Ferrari</strong>
        </p>
      </div>
    );
  }
}

export default HomePage;
