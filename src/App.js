import 'babel-polyfill';
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styled from 'styled-components';

import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import store from './store';
import HomePage from './Containers/HomePage';
import Users from './Containers/Users';
import Login from './Containers/Users/Login';
import CreateUser from './Containers/Users/CreateUser';
import Repairs from './Containers/Repairs';
import Repair from './Containers/Repairs/Repair';
import Header from './Containers/HomePage/Header';
import client from './apollo';

const Wrapper = styled.div`
  max-width: 1020px;
  margin: 80px auto;
  padding: 0 10px;
`;

const App = () => (
  <ApolloProvider client={client} store={store}>
    <MuiThemeProvider>
      <Router>
        <Wrapper>
          <Header />
          <Route path="/" exact component={HomePage} />
          <Route path="/login" exact component={Login} />
          <Route path="/users" exact component={Users} />
          <Route path="/users/new" exact component={CreateUser} />
          <Route path="/repairs" exact component={Repairs} />
          <Route path="/repairs/:id" exact component={Repair} />
        </Wrapper>
      </Router>
    </MuiThemeProvider>
  </ApolloProvider>
);

export default App;

