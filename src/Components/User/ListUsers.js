import React, { PropTypes } from 'react';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import styled from 'styled-components';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const ButtonMargin = styled.div`
  margin: 20px;
`;

const UserRow = ({ user, onDelete, onToggle }) => (
  <TableRow>
    <TableRowColumn>{user.email}</TableRowColumn>
    <TableRowColumn>
      <Toggle
        name="isManager"
        label="Manager"
        toggled={user.isManager}
        onToggle={onToggle}
      />
    </TableRowColumn>
    <TableRowColumn>
      <RaisedButton
        label="Delete"
        primary
        onClick={() => onDelete(user.id)}
      />
    </TableRowColumn>
  </TableRow>
);


UserRow.propTypes = {
  user: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
};

const ListUsers = ({ users, onDelete, onToggle, onCreate }) => (
  <div>
    <h2>Create User</h2>
    <Table>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>E-mail</TableHeaderColumn>
          <TableHeaderColumn>Role</TableHeaderColumn>
          <TableHeaderColumn>Delete</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
        {users.map((user) => (
          <UserRow
            key={user.id}
            user={user}
            onDelete={onDelete}
            onToggle={() => onToggle(user.id, user.isManager)}
          />)
        )}
      </TableBody>
    </Table>
    <ButtonMargin>
      <FloatingActionButton
        onClick={onCreate}
      >
        <ContentAdd />
      </FloatingActionButton>
    </ButtonMargin>
  </div>
);

ListUsers.propTypes = {
  users: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
};


export default ListUsers;
