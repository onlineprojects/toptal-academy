import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const LoginForm = ({ email, password, onLogin, onChange }) => (
  <div>
    <h2>Login</h2>
    <form>
      <TextField
        name="email"
        hintText="Enter Email"
        floatingLabelText="Email"
        value={email}
        fullWidth
        onChange={onChange}
      />
      <TextField
        name="password"
        hintText="Enter Password"
        value={password}
        fullWidth
        type="password"
        onChange={onChange}
      />
      <RaisedButton
        label="login ..."
        primary
        onClick={onLogin}
      />
    </form>
  </div>
);

LoginForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onLogin: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default LoginForm;
