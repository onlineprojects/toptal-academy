import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const CreateUserForm = ({ email, password, onSave, onChange }) => (
  <div>
    <h2>Create User</h2>
    <form>
      <TextField
        name="email"
        hintText="Enter Email"
        floatingLabelText="Email"
        value={email}
        fullWidth
        onChange={onChange}
      />
      <TextField
        name="password"
        hintText="Enter Password"
        value={password}
        fullWidth
        type="password"
        onChange={onChange}
      />
      <RaisedButton
        label="Create user"
        primary
        onClick={onSave}
      />
    </form>
  </div>
);

CreateUserForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CreateUserForm;
