import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const Comment = ({ comment }) => (
  <li>
    <div>User: {comment.user.email}</div>
    <div>Msg: {comment.comment}</div>
    <br />
  </li>
);

Comment.propTypes = {
  comment: PropTypes.object.isRequired,
};


const Comments = ({ comments, onChange, onCreate }) => (
  <div>
    <h2>Commnets</h2>
    <ul>
      {comments.map((comment) => <Comment key={comment.id} comment={comment} />)}
    </ul>
    <form>
      <TextField
        name="comment"
        hintText="Enter Comment Text"
        floatingLabelText="New Comment"
        fullWidth
        onChange={onChange}
      />
      <RaisedButton
        label="Send new comment ..."
        primary
        onClick={onCreate}
      />
    </form>
  </div>
);

Comments.propTypes = {
  comments: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
};
Comments.defaultProps = {
  comments: [],
};

export default Comments;
