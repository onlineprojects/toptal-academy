import React, { PropTypes } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const MenuGroup = styled.div`
  display: flex;
  justify-content: flex-start;
`;

const MenuItem = styled.div`
  flex: 1;
  font-size: 20px;
`;

const MainHeader = ({ user, authRole, logout }) => (
  <MenuGroup>
    <MenuItem><Link to={'/'}>Home</Link></MenuItem>
    { (authRole !== undefined) && // logged
      <MenuItem><Link to={'/repairs'}>Repairs</Link></MenuItem>
    }
    { (authRole === true) && // isManger
      <MenuItem><Link to={'/users'}>List Users</Link></MenuItem>
    }
    { (authRole === undefined) && // not logged
      <MenuItem><Link to={'/users/new'}>Create User</Link></MenuItem>
    }
    { (authRole === undefined) && // not logged
      <MenuItem><Link to={'/login'}>Login</Link></MenuItem>
    }
    { (authRole !== undefined) && // logged
      <MenuItem><a href="#" onClick={logout}>Logout ({user.email})</a></MenuItem>
    }
  </MenuGroup>
);

MainHeader.propTypes = {
  authRole: PropTypes.bool,
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
};

MainHeader.defaultProps = {
  authRole: undefined,
  user: { id: undefined, email: undefined, isManager: undefined },
};

export default MainHeader;
