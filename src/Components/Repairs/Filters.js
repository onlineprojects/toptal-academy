import React, { PropTypes } from 'react';
import DatePicker from 'material-ui/DatePicker';
import Slider from 'material-ui/Slider';
import AutoComplete from 'material-ui/AutoComplete';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';
import styled from 'styled-components';

const FilterCenter = styled.div`
  margin: 20px auto;
  width: 500px;
`;

const Filters = ({ filters, users, onChange, onSelectUser, isManager }) => (
  <div>
    <FilterCenter>
      <DatePicker
        hintText="Enter end date"
        floatingLabelText="Filter Date up to:"
        container="inline"
        mode="landscape"
        name="dateEnd"
        value={filters.dateEnd}
        onChange={(e, v) => onChange('dateEnd', v)}
      />
    </FilterCenter>
    <Divider />
    <FilterCenter>
      <br />
      <div>Time Start and End (from {filters.timeStart} to {filters.timeEnd})</div>
      <Slider
        name="timeStart"
        min={0}
        max={23}
        step={1}
        value={filters.timeStart}
        onChange={(e, v) => onChange('timeStart', v)}
      />
      <Slider
        name="timeEnd"
        min={0}
        max={23}
        step={1}
        value={filters.timeEnd}
        onChange={(e, v) => onChange('timeEnd', v)}
      />
    </FilterCenter>
    <Divider />
    { isManager &&
      <FilterCenter>
        <AutoComplete
          name="userId"
          hintText="Type to select a Client"
          floatingLabelText="Filter Client"
          filter={AutoComplete.caseInsensitiveFilter}
          dataSource={users || []}
          dataSourceConfig={{ text: 'email', value: 'id' }}
          maxSearchResults={5}
          onNewRequest={onSelectUser}
        />
        <Divider />
      </FilterCenter>
    }
    <FilterCenter>
      <RadioButtonGroup
        name="isComplete"
        defaultSelected={undefined}
        onChange={(e, v) =>
          onChange('isComplete', ((v === undefined) ? undefined : (v === 'true')))}
      >
        <RadioButton
          value={undefined}
          label="All"
        />
        <RadioButton
          value="false"
          label="Not Complete"
        />
        <RadioButton
          value="true"
          label="Complete!"
        />
      </RadioButtonGroup>
    </FilterCenter>
    <Divider />
    <Divider />
    <br />
  </div>
);

Filters.propTypes = {
  filters: PropTypes.object.isRequired,
  users: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  onSelectUser: PropTypes.func.isRequired,
  isManager: PropTypes.bool,
};

Filters.defaultProps = {
  isManager: undefined,
};

export default Filters;
