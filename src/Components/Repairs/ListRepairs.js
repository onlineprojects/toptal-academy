import React, { PropTypes } from 'react';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import styled from 'styled-components';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const ButtonMargin = styled.div`
  margin: 20px;
`;

const RepairRow = ({ repair, onEdit }) => (
  <TableRow>
    <TableRowColumn>{repair.user.email}</TableRowColumn>
    <TableRowColumn>{repair.scheduleDate.substring(0, 10)}</TableRowColumn>
    <TableRowColumn>{repair.scheduleTime}</TableRowColumn>
    <TableRowColumn>
      <Toggle
        name="isComplete"
        label=""
        toggled={repair.isComplete}
        disabled
      />
    </TableRowColumn>
    <TableRowColumn>
      <Toggle
        name="isApproved"
        label=""
        toggled={repair.isApproved}
        disabled
      />
    </TableRowColumn>
    <TableRowColumn>
      <RaisedButton
        label="Edit"
        primary
        onClick={onEdit}
      />
    </TableRowColumn>
  </TableRow>
);


RepairRow.propTypes = {
  repair: PropTypes.object.isRequired,
  onEdit: PropTypes.func.isRequired,
};

const ListRepairs = ({ repairs, onEdit, isManager }) => (
  <div>
    <Table>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>User e-mail</TableHeaderColumn>
          <TableHeaderColumn>Date</TableHeaderColumn>
          <TableHeaderColumn>Time</TableHeaderColumn>
          <TableHeaderColumn>is Complete</TableHeaderColumn>
          <TableHeaderColumn>is Approved</TableHeaderColumn>
          <TableHeaderColumn>Edit</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
        {repairs.map((repair) => (
          <RepairRow
            key={repair.id}
            repair={repair}
            onEdit={() => onEdit(repair.id)}
          />)
        )}
      </TableBody>
    </Table>
    { isManager &&
      <ButtonMargin>
        <FloatingActionButton
          onClick={() => onEdit('new')}
        >
          <ContentAdd />
        </FloatingActionButton>
      </ButtonMargin>
    }
  </div>
);

ListRepairs.propTypes = {
  repairs: PropTypes.array.isRequired,
  onEdit: PropTypes.func.isRequired,
  isManager: PropTypes.bool,
};

ListRepairs.defaultProps = {
  isManager: undefined,
};

export default ListRepairs;
