import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import AutoComplete from 'material-ui/AutoComplete';

const RepairForm = ({ repair, users, user, onSave, onChange, onSelectUser }) => (
  <div>
    <h2>{repair.id ? 'Edit Repair' : 'Create Repair'}</h2>
    <form>
      { user.isManager &&
        <div>
          <TextField
            name="userEmail"
            floatingLabelText="Client"
            value={(repair.user) ? repair.user.email : ''}
            disabled
          />
          <AutoComplete
            name="userId"
            hintText="Type to select a new Client"
            floatingLabelText="Change Client"
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={users || []}
            dataSourceConfig={{ text: 'email', value: 'id' }}
            maxSearchResults={5}
            onNewRequest={onSelectUser}
          />
        </div>
      }
      <DatePicker
        hintText="Schedule Date"
        floatingLabelText="Schedule Date"
        container="inline"
        mode="landscape"
        name="dtScheduleDate"
        value={repair.dtScheduleDate}
        onChange={onChange}
        disabled={!(user.isManager)}
      />
      <TextField
        name="scheduleTime"
        floatingLabelText="Schedule Time"
        hintText="Enter Time"
        value={repair.scheduleTime || ''}
        onChange={onChange}
        min={0}
        max={23}
        type="number"
        disabled={!(user.isManager)}
      />
      <Toggle
        name="isComplete"
        label="Is Complete?"
        toggled={repair.isComplete}
        onToggle={onChange}
        labelPosition="right"
        disabled={(!user.isManager && repair.alreadyComplete)}
      />
      <RaisedButton
        label={repair.id ? 'Save Repair' : 'Create Repair'}
        primary
        onClick={onSave}
        disabled={(!user.isManager && repair.alreadyComplete)}
      />
    </form>
  </div>
);

RepairForm.propTypes = {
  repair: PropTypes.object.isRequired,
  users: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSelectUser: PropTypes.func.isRequired,
};

export default RepairForm;
