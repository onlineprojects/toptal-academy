import ApolloClient, { createNetworkInterface } from 'apollo-client';

const networkInterface = createNetworkInterface({
  uri: 'https://api.graph.cool/simple/v1/cj7nvpqye01qt0121ez0wq1ik',
});

const config = {
  dataIdFromObject: (o) => o.id,
  networkInterface,
  queryDeduplication: true,
};

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {}; // eslint-disable-line no-param-reassign
    }
    if (localStorage.getItem('authIdToken')) {
      req.options.headers.authorization = // eslint-disable-line no-param-reassign
        `Bearer ${localStorage.getItem('authIdToken')}`;
    }
    next();
  },
}]);

export default new ApolloClient(config);
