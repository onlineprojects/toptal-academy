import { combineReducers } from 'redux';
import repairsStore from './Containers/Repairs/reducers';
import usersStore from './Containers/Users/reducers';

export default combineReducers({
  repairsStore,
  usersStore,
});
