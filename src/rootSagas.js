import { all } from 'redux-saga/effects';

import {
  createRepairSaga,
  updateRepairSaga,
  deleteRepairSaga,
  createCommentSaga,
  deleteCommentSaga,
} from './Containers/Repairs/sagas';

import {
  createUserSaga,
  loginUserSaga,
  deleteUserSaga,
  changeUserSaga,
} from './Containers/Users/sagas';

export default function* rootSagas() {
  yield all([
    createRepairSaga(),
    updateRepairSaga(),
    deleteRepairSaga(),
    createCommentSaga(),
    deleteCommentSaga(),
    createUserSaga(),
    loginUserSaga(),
    deleteUserSaga(),
    changeUserSaga(),
  ]);
}
