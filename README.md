

to run local:
  - yarn && yarn start

Manager User:
email: test@test.com
password: Rio@2016




GraphCool schema:
# projectId: cj7nvpqye01qt0121ez0wq1ik
# version: 26

type Comment implements Node {
  createdAt: DateTime!
  id: ID! @isUnique
  updatedAt: DateTime!
  comment: String
  repair: Repair! @relation(name: "CommentOnRepair")
  user: User! @relation(name: "CommentOnUser")
}

type File implements Node {
  contentType: String!
  createdAt: DateTime!
  id: ID! @isUnique
  name: String!
  secret: String! @isUnique
  size: Int!
  updatedAt: DateTime!
  url: String! @isUnique
}

type User implements Node {
  createdAt: DateTime!
  id: ID! @isUnique
  updatedAt: DateTime!
  repairs: [Repair!]! @relation(name: "RepairOnUser")
  comments: [Comment!]! @relation(name: "CommentOnUser")
  email: String @isUnique
  isManager: Boolean @defaultValue(value: false)
  password: String
}

type Repair implements Node {
  createdAt: DateTime!
  id: ID! @isUnique
  updatedAt: DateTime!
  scheduleDate: DateTime!
  user: User @relation(name: "RepairOnUser")
  comments: [Comment!]! @relation(name: "CommentOnRepair")
  scheduleTime: Int!
  isApproved: Boolean @defaultValue(value: false)
  isComplete: Boolean! @defaultValue(value: false)
}